<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 28.04.2017
 * Time: 10:12
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Enduser;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{

    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    public function load(ObjectManager  $manager)
    {

        $user = new Enduser();
        $us = new Enduser();

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '1');
        $user->setPassword($password);
        $user->setUsername('admin');
        $user->getSalt(md5(uniqid()));
        $user->setName('Burhan Yılmaz');
        $user->setMail('b@gmail.com');
        $user->setSex(true);
        $user->setCity('1');


        $password = $encoder->encodePassword($us, '1');
        $us->setPassword($password);
        $us->setUsername('b');
        $us->getSalt(md5(uniqid()));
        $us->setName('Burhan Yılmaz');
        $us->setMail('by@gmail.com');
        $us->setSex(true);
        $us->setCity('2');

        $manager->persist($us);
        $manager->persist($user);
        $manager->flush();


    }

}