<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 20.05.2017
 * Time: 08:51
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\BatchDownloadRequest;
use AppBundle\Entity\CloudFile;
use AppBundle\Entity\Enduser;
use AppBundle\Entity\PendingEmail;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class TestUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager  $manager)
    {

        $user = new Enduser();
        $us1 = new Enduser();
        $us = new Enduser();

        $encoder = $this->container->get('security.password_encoder');
        $password = $encoder->encodePassword($user, '1');
        $user->setPassword($password);
        $user->setUsername('mehmet');
        $user->getSalt(md5(uniqid()));
        $user->setName('Mehmet Demir');
        $user->setMail('m@gmail.com');
        $user->setSex(true);
        $user->setCity('1');
        $preferences=array('sharing_enabled'=>false,'sharing_notification_enabled'=>false,'api_enabled'=>false);
        $user->setPreferences($preferences);


        $password = $encoder->encodePassword($us, '1');
        $us->setPassword($password);
        $us->setUsername('ayse');
        $us->getSalt(md5(uniqid()));
        $us->setName('Ayşe Bakırcı');
        $us->setMail('ay@gmail.com');
        $us->setSex(false);
        $us->setCity('2');
        $us->setMaidenName('Bakır');
        $manager->persist($us);
        $manager->persist($user);
        $manager->flush();

        $password = $encoder->encodePassword($us1, '1');
        $us1->setPassword($password);
        $us1->setUsername('mehmetdemir');
        $us1->getSalt(md5(uniqid()));
        $us1->setName('Mehmet Demir');
        $us1->setMail('mehmetdemir@mehmetdemir.com');
        $us1->setSex(true);
        $us1->setCity('52');
        $manager->persist($us1);
        $manager->flush();


        $cloudFile=new CloudFile();
        $cloudFile->setFileName('59103ae2c4b06wwf-logo-design.jpg');
        $cloudFile->setUniqName('5926ed431bdb2setting.png');
        $cloudFile->setUser($user);
        $cloudFile->setFileSize(9);
        $cloudFile->setFav(0);
        $manager->persist($cloudFile);
        $manager->flush();
        $file=$cloudFile->getId();

        $cF=new CloudFile();
        $cF->setFileName('59103ae2c4b06wwf-logo-design.jpg');
        $cF->setUniqName('5926ed3e3c538authhome.png');
        $cF->setUser($user);
        $cF->setFileSize(9);
        $cF->setFav(0);
        $manager->persist($cF);
        $manager->flush();
        $cF->getId();

        $file1=$cF->getId();

        $cF1=new CloudFile();
        $cF1->setFileName('59103ae2c4b06wwf-logo-design.jpg');
        $cF1->setUniqName('5926ed394866ddelete.png');
        $cF1->setUser($user);
        $cF1->setFileSize(9);
        $cF1->setFav(0);
        $manager->persist($cF1);
        $manager->flush();
        $cF1->getId();
        $file2=$cF1->getId();


        $cF2=new CloudFile();
        $cF2->setFileName('59103ae2c4b06wwf-logo-design.jpg');
        $cF2->setUniqName('5926c22479214delete.png');
        $cF2->setUser($us);
        $cF2->setFileSize(9);
        $cF2->setFav(0);
        $manager->persist($cF2);
        $manager->flush();
        $cF2->getId();
        $file3=$cF2->getId();




        $batchDown=new BatchDownloadRequest();
        $batchDown->setOwner($user);
        $batchDown->setFiles(array("$file","$file1"));
        $manager->persist($batchDown);
        $manager->flush();



        #Dosya gönderme için

        $file=new PendingEmail();
        $file->setAttachment($cF->getUniqName());
        $file->setBody('Test Sending File');
        $file->setFromEmail($user->getMail());
        $file->setToEmail($user->getMail());
        $file->setReplyToEmail($user->getMail());
        $file->setCcEmail($user->getMail());
        $file->setSubject('Test Sending File');
        $manager->persist($file);
        $manager->flush();

    }

}