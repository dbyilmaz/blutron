<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\Email;
use Symfony\Component\Console\Command\Command ;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Process\ProcessBuilder;


class SendMailCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('SendMail')
            ->setDescription('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         * @var Email $emailServices
         */
        $emailServices=$this->getContainer()->get('app.mail');
        $emailServices->sendMail();
        $output->writeln('Mail Gitti');

    }

}
