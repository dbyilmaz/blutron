<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use AppBundle\Services\FileZip;
use Symfony\Component\Console\Command\Command ;
class BatchDownloadDispatcherCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('BatchDownloadDispatcher')
            ->setDescription('...')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /*
         * @var FileZip $filezipServices
         */
        $filezipServices=$this->getContainer()->get('app.zip');
        $filezipServices->sendFiles();
        $output->writeln('Mail Gitti');
    }

}
