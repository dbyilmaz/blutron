<?php

namespace AppBundle\Entity;


use AppBundle\Entity\Enduser;
use AppBundle\Entity\CloudFile;
use Doctrine\Common\Collections\ArrayCollection;

use Doctrine\ORM\Mapping as ORM;

/**
 * SharedCloudFile
 *
 * @ORM\Table(name="shared_cloud_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SharedCloudFileRepository")
 */
class SharedCloudFile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\CloudFile", inversedBy="sharedFiles")
     * @ORM\JoinColumn(name="file", referencedColumnName="id")
     *
     *
     *
     *
     */

    private $file;


    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Enduser", inversedBy="shareOwners")
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     *
     *
     *
     */

    private $owner;


    /**
     *
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Enduser", inversedBy="shareTargets")
     * @ORM\JoinColumn(name="target", referencedColumnName="id")
     *
     */
    private $target;

    /**
     * @var integer
     * @ORM\Column( name="fileDown",type="integer" ,options={"default":0})
     */
    private $fileDown=0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fileDown
     *
     * @param integer $fileDown
     *
     * @return SharedCloudFile
     */
    public function setFileDown($fileDown)
    {
        $this->fileDown = $fileDown;

        return $this;
    }

    /**
     * Get fileDown
     *
     * @return integer
     */
    public function getFileDown()
    {
        return $this->fileDown;
    }

    /**
     * Set file
     *
     * @param \AppBundle\Entity\CloudFile $file
     *
     * @return SharedCloudFile
     */
    public function setFile(\AppBundle\Entity\CloudFile $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get file
     *
     * @return \AppBundle\Entity\CloudFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\Enduser $owner
     *
     * @return SharedCloudFile
     */
    public function setOwner(\AppBundle\Entity\Enduser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\Enduser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set target
     *
     * @param \AppBundle\Entity\Enduser $target
     *
     * @return SharedCloudFile
     */
    public function setTarget(\AppBundle\Entity\Enduser $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \AppBundle\Entity\Enduser
     */
    public function getTarget()
    {
        return $this->target;
    }
}
