<?php

namespace AppBundle\Entity;
use AppBundle\Entity\Enduser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\DateTime;


/**
 * CloudFile
 *
 * @ORM\Table(name="cloud_file")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CloudFileRepository")
 */
class CloudFile
{
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->sharedFiles = new \Doctrine\Common\Collections\ArrayCollection();
        $this->uploadedAt=new\DateTime('now');
    }

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var /DateTime
     *
     * @ORM\Column(name="uploaded_at",type="datetime")
     */
    private $uploadedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="fileSize", type="integer")
     */
    private $fileSize;



    /**
     * @var string
     *
     * @ORM\Column(name="fileName", type="string", length=255)
     */
    private $fileName;




    /**
     * @var string
     *
     * @ORM\Column(name="uniqName", type="string", length=255 ,unique=true)
     */
    private $uniqName;



    /**
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Enduser", inversedBy="files")
     * @ORM\JoinColumn(name="user", referencedColumnName="id")
     */

    private  $user;



    /**
     * @ORM\Column(type="boolean", name="fav", options={"default": false})
     */

    private $fav;


    /**
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SharedCloudFile", mappedBy="file")
     *
     *
     */
    private $sharedFiles;




    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uploadedAt
     *
     * @param \DateTime $uploadedAt
     *
     * @return CloudFile
     */
    public function setUploadedAt($uploadedAt)
    {
        $this->uploadedAt = $uploadedAt;

        return $this;
    }

    /**
     * Get uploadedAt
     *
     * @return \DateTime
     */
    public function getUploadedAt()
    {
        return $this->uploadedAt;
    }

    /**
     * Set fileSize
     *
     * @param integer $fileSize
     *
     * @return CloudFile
     */
    public function setFileSize($fileSize)
    {
        $this->fileSize = $fileSize;

        return $this;
    }

    /**
     * Get fileSize
     *
     * @return int
     */
    public function getFileSize()
    {
        return $this->fileSize;
    }



    /**
     * Set fileName
     *
     * @param string $fileName
     *
     * @return CloudFile
     */
    public function setFileName($fileName)
    {
        $this->fileName = $fileName;

        return $this;
    }

    /**
     * Get fileName
     *
     * @return string
     */
    public function getFileName()
    {
        return $this->fileName;
    }


    /**
     * Set uniqName
     *
     * @param string $uniqName
     *
     * @return CloudFile
     */
    public function setUniqName($uniqName)
    {
        $this->uniqName = $uniqName;

        return $this;
    }

    /**
     * Get uniqName
     *
     * @return string
     */
    public function getUniqName()
    {
        return $this->uniqName;
    }




    /**
     * Set user
     *
     * @var Enduser
     *
     * @return Enduser
     */
    public  function setUser($user)
    {
        $this->user=$user;
        return $this;
    }

    /**
     * Get user
     *
     * @return Enduser
     */
    public  function getUser()
    {
        return $this->user;


    }


    /**
     * Set fav
     *
     * @param boolean $uniqName
     *
     * @return CloudFile
     */
    public function setFav($fav)
    {
        $this->fav = $fav;

        return $this;
    }

    /**
     * Get fav
     *
     * @return boolean
     */
    public function getFav()
    {
        return $this->fav;
    }






    /**
     * Add sharedFile
     *
     * @param \AppBundle\Entity\SharedCloudFile $sharedFile
     *
     * @return CloudFile
     */
    public function addSharedFile(\AppBundle\Entity\SharedCloudFile $sharedFile)
    {
        $this->sharedFiles[] = $sharedFile;

        return $this;
    }

    /**
     * Remove sharedFile
     *
     * @param \AppBundle\Entity\SharedCloudFile $sharedFile
     */
    public function removeSharedFile(\AppBundle\Entity\SharedCloudFile $sharedFile)
    {
        $this->sharedFiles->removeElement($sharedFile);
    }

    /**
     * Get sharedFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSharedFiles()
    {
        return $this->sharedFiles;
    }
}
