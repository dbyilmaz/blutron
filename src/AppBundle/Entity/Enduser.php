<?php

namespace AppBundle\Entity;

use AppBundle\Entity\CloudFile;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;


/**
 * Enduser
 *
 * @ORM\Table(name="enduser")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EnduserRepository")
 * @UniqueEntity(fields="mail", message="Girmiş olduğunuz email kullanılmaktadır.")
 * @UniqueEntity(fields="username", message="Girmiş olduğunuz kullanıcı adı kullanılmaktadır.")
 */
class Enduser implements   UserInterface, \Serializable
{



    public function __construct()
    {
        $this->files = new ArrayCollection();

        $preferences=array('sharing_enabled'=>true,'sharing_notification_enabled'=>true,'api_enabled'=>false);
        $this->setPreferences($preferences);
        $this->setLastLoggedIn(new \DateTime('now'));

    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Assert\Length(min="2",minMessage="En az 1 karakter girmelisiniz.")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, unique=true)
     *
     */
    private $username;

    /**
     * @var string
     * @ORM\Column(name="password", type="string", length=255)
     */
    private $password;


    /**
     * @var string
     * @ORM\Column(name="mail", type="string", unique=true , nullable=false )
     */


    private $mail;



    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lastLoggedIn", type="datetime", nullable=true)
     */
    private $lastLoggedIn;


    /**
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CloudFile", mappedBy="user")
     */
    private $files;

    /**
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\BatchDownloadRequest", mappedBy="owner")
     */
    private $batchFiles;


    /**
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SharedCloudFile", mappedBy="owner")
     *
     */
    private $shareOwners;



    /**
     *
     *
     * @ORM\Column(name="sex", type="boolean")
     */
    private $sex;


    /**
     *
     * @var string
     * @ORM\Column(name="city", type="text")
     */
    private $city;


    /**
     * @var array
     *
     * @ORM\Column(name="preferences", type="json_array")
     */
    private $preferences;

    /**
     *
     * @var string
     * @ORM\Column(name="maiden_name", type="text", nullable=true)
     *
     */
    private $maidenName;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\SharedCloudFile", mappedBy="target")
     */
    private $shareTargets;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Enduser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return Enduser
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Enduser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return Enduser
     */
    public function setNewPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getNewPassword()
    {
        return $this->password;
    }


    /**
     * Set mail
     *
     * @param string $mail
     *
     * @return Enduser
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }





    /**
     * Set lastLoggedIn
     *
     * @param \DateTime $lastLoggedIn
     *
     * @return Enduser
     */
    public function setLastLoggedIn($lastLoggedIn)
    {
        $this->lastLoggedIn = $lastLoggedIn;

        return $this;
    }

    /**
     * Get lastLoggedIn
     *
     * @return \DateTime
     */
    public function getLastLoggedIn()
    {
        return $this->lastLoggedIn;
    }


    public function getSalt()
    {
        // you *may* need a real salt depending on your encoder
        // see section on salt below
        return null;
    }



    public function getRoles()
    {
        return array('ROLE_USER');
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->username,
            $this->mail,
            $this->password,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->username,
            $this->mail,
            $this->password,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized);
    }

    /**
     * Add file
     *
     * @param \AppBundle\Entity\CloudFile $file
     *
     * @return Enduser
     */
    public function addFile(\AppBundle\Entity\CloudFile $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \AppBundle\Entity\CloudFile $file
     */
    public function removeFile(\AppBundle\Entity\CloudFile $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Add shareOwner
     *
     * @param \AppBundle\Entity\SharedCloudFile $shareOwner
     *
     * @return Enduser
     */
    public function addShareOwner(\AppBundle\Entity\SharedCloudFile $shareOwner)
    {
        $this->shareOwners[] = $shareOwner;

        return $this;
    }

    /**
     * Remove shareOwner
     *
     * @param \AppBundle\Entity\SharedCloudFile $shareOwner
     */
    public function removeShareOwner(\AppBundle\Entity\SharedCloudFile $shareOwner)
    {
        $this->shareOwners->removeElement($shareOwner);
    }

    /**
     * Get shareOwners
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShareOwners()
    {
        return $this->shareOwners;
    }

    /**
     * Add shareTarget
     *
     * @param \AppBundle\Entity\SharedCloudFile $shareTarget
     *
     * @return Enduser
     */
    public function addShareTarget(\AppBundle\Entity\SharedCloudFile $shareTarget)
    {
        $this->shareTargets[] = $shareTarget;

        return $this;
    }

    /**
     * Remove shareTarget
     *
     * @param \AppBundle\Entity\SharedCloudFile $shareTarget
     */
    public function removeShareTarget(\AppBundle\Entity\SharedCloudFile $shareTarget)
    {
        $this->shareTargets->removeElement($shareTarget);
    }

    /**
     * Get shareTargets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShareTargets()
    {
        return $this->shareTargets;
    }

    /**
     * Add batchFile
     *
     * @param \AppBundle\Entity\BatchDownloadRequest $batchFile
     *
     * @return Enduser
     */
    public function addBatchFile(\AppBundle\Entity\BatchDownloadRequest $batchFile)
    {
        $this->batchFiles[] = $batchFile;

        return $this;
    }

    /**
     * Remove batchFile
     *
     * @param \AppBundle\Entity\BatchDownloadRequest $batchFile
     */
    public function removeBatchFile(\AppBundle\Entity\BatchDownloadRequest $batchFile)
    {
        $this->batchFiles->removeElement($batchFile);
    }

    /**
     * Get batchFiles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBatchFiles()
    {
        return $this->batchFiles;
    }

    /**
     * Set sex
     *
     * @param string $sex
     *
     * @return Enduser
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return boolean
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Enduser
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }


    /**
     * Set $preferences
     *
     * @param array $preferences
     *
     * @return Enduser
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;

        return $this;
    }

    /**
     * Get preferences
     *
     * @return array
     */
    public function getPreferences()
    {
        return $this->preferences;
    }





    /**
     * Set maidenName
     *
     * @param string $maidenName
     *
     * @return Enduser
     */
    public function setMaidenName($maidenName)
    {
        $this->maidenName = $maidenName;

        return $this;
    }

    /**
     * Get maidenName
     *
     * @return string
     */
    public function getMaidenName()
    {
        return $this->maidenName;
    }
}
