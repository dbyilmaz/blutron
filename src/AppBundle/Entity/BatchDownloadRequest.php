<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * BatchDownloadRequest
 *
 * @ORM\Table(name="batch_download_request")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BatchDownloadRequestRepository")
 */
class BatchDownloadRequest
{
    public function __construct()
    {
        $this->createdAt=new\DateTime('now');
        $this->error=null;
        $this->status=0;
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     *
     * @ORM\JoinColumn(name="owner", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Enduser", inversedBy="batchFiles")
     */
    private $owner;

    /**
     * @var array
     *
     * @ORM\Column(name="files", type="json_array")
     */
    private $files;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="error", type="string", length=1000, nullable=true)
     */
    private $error;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set files
     *
     * @param array $files
     *
     * @return BatchDownloadRequest
     */
    public function setFiles($files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * Get files
     *
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return BatchDownloadRequest
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return BatchDownloadRequest
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentedAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return BatchDownloadRequest
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set error
     *
     * @param string $error
     *
     * @return BatchDownloadRequest
     */
    public function setError($error)
    {
        $this->error = $error;

        return $this;
    }

    /**
     * Get error
     *
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * Set owner
     *
     * @param \AppBundle\Entity\Enduser $owner
     *
     * @return BatchDownloadRequest
     */
    public function setOwner(\AppBundle\Entity\Enduser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \AppBundle\Entity\Enduser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set sentdAt
     *
     * @param \DateTime $sentdAt
     *
     * @return BatchDownloadRequest
     */
    public function setSentdAt($sentdAt)
    {
        $this->sentdAt = $sentdAt;

        return $this;
    }

    /**
     * Get sentdAt
     *
     * @return \DateTime
     */
    public function getSentdAt()
    {
        return $this->sentdAt;
    }
}
