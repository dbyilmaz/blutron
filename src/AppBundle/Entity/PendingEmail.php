<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmailStore
 *
 * @ORM\Table(name="pending_email")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PendingEmailRepository")
 */
class PendingEmail
{

    public function __construct()
    {
        $this->setCratedAt(new\DateTime('now'));
        $this->setStatus(0);
        $this->setSentAt(null);
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="fromEmail", type="string", length=255)
     */
    private $fromEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="toEmail", type="string", length=255)
     */
    private $toEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="ccEmail", type="string", length=255, nullable=true)
     */
    private $ccEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="bccEmail", type="string", length=255, nullable=true)
     */
    private $bccEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="replyToEmail", type="string", length=255)
     */
    private $replyToEmail;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", length=1000)
     */
    private $body;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="crated_at", type="datetime")
     */
    private $cratedAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sent_at", type="datetime", nullable=true)
     */
    private $sentAt;

    /**
     * @var string
     *
     * @ORM\Column(name="attachment", type="string", length=255, nullable=true)
     */
    private $attachment;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subject
     *
     * @param string $subject
     *
     * @return PendingEmail
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set fromEmail
     *
     * @param string $fromEmail
     *
     * @return PendingEmail
     */
    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    /**
     * Get fromEmail
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    /**
     * Set toEmail
     *
     * @param string $toEmail
     *
     * @return PendingEmail
     */
    public function setToEmail($toEmail)
    {
        $this->toEmail = $toEmail;

        return $this;
    }

    /**
     * Get toEmail
     *
     * @return string
     */
    public function getToEmail()
    {
        return $this->toEmail;
    }

    /**
     * Set ccEmail
     *
     * @param string $ccEmail
     *
     * @return PendingEmail
     */
    public function setCcEmail($ccEmail)
    {
        $this->ccEmail = $ccEmail;

        return $this;
    }

    /**
     * Get ccEmail
     *
     * @return string
     */
    public function getCcEmail()
    {
        return $this->ccEmail;
    }

    /**
     * Set bccEmail
     *
     * @param string $bccEmail
     *
     * @return PendingEmail
     */
    public function setBccEmail($bccEmail)
    {
        $this->bccEmail = $bccEmail;

        return $this;
    }

    /**
     * Get bccEmail
     *
     * @return string
     */
    public function getBccEmail()
    {
        return $this->bccEmail;
    }

    /**
     * Set replyToEmail
     *
     * @param string $replyToEmail
     *
     * @return PendingEmail
     */
    public function setReplyToEmail($replyToEmail)
    {
        $this->replyToEmail = $replyToEmail;

        return $this;
    }

    /**
     * Get replyToEmail
     *
     * @return string
     */
    public function getReplyToEmail()
    {
        return $this->replyToEmail;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return PendingEmail
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set cratedAt
     *
     * @param \DateTime $cratedAt
     *
     * @return PendingEmail
     */
    public function setCratedAt($cratedAt)
    {
        $this->cratedAt = $cratedAt;

        return $this;
    }

    /**
     * Get cratedAt
     *
     * @return \DateTime
     */
    public function getCratedAt()
    {
        return $this->cratedAt;
    }

    /**
     * Set sentAt
     *
     * @param \DateTime $sentAt
     *
     * @return PendingEmail
     */
    public function setSentAt($sentAt)
    {
        $this->sentAt = $sentAt;

        return $this;
    }

    /**
     * Get sentAt
     *
     * @return \DateTime
     */
    public function getSentAt()
    {
        return $this->sentAt;
    }

    /**
     * Set attachment
     *
     * @param string $attachment
     *
     * @return PendingEmail
     */
    public function setAttachment($attachment)
    {
        $this->attachment = $attachment;

        return $this;
    }

    /**
     * Get attachment
     *
     * @return string
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return PendingEmail
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }
}
