<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enduser;
use AppBundle\Form\ChangePassType;
use AppBundle\Form\ForgetPassType;
use AppBundle\Form\SettingsType;
use AppBundle\Form\UserLoginType;
use AppBundle\Form\UserRegisterType;
use AppBundle\Services\EnduserPreferences;
use AppBundle\Services\Messages;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class UserController extends Controller
{

    public function indexAction(Request $request)
    {
        /*Kullanıcı girişi için gerekli*/
        $authenticationUtils = $this->get('security.authentication_utils');

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        /*Giriş yapmış kullanıcı bilgilerini çekmeyi sağlar. Bunun dışında kullanıcı rolu vb..*/
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /**
         * @var $form UserLoginType
         */
        $form=new UserLoginType();
        $form=$this->createForm(UserLoginType::class);

        // replace this example code with whatever you need
        return $this->render('AppBundle::user/index.html.twig', array(
            'last_username' => $lastUsername,
            'error'         => $error,
            'user'          => $user,
            'form'         => $form->createView(),
        ));


    }

    public function registerAction(Request $request)
    {

        /**
         * @var  Form $registerForm
         * @var Enduser $user
         */
        $user=new Enduser();
        $registerForm=$this->createForm(UserRegisterType::class,$user);
        $registerForm=$registerForm->handleRequest($request);

        if ($registerForm->isSubmitted()&&$registerForm->isValid())
        {
            $pass=$this->encodePass($user,$registerForm->get('password')->getData());
            $user->setUsername($registerForm->get('username')->getData());
            $user->setPassword($pass);

            if($registerForm->get('sex')->getData()==0){
                $user->setMaidenName($registerForm->get('sex')->getData());
            }

            $r=$this->getDoctrine()->getManager();
            $r->persist($user);
            $r->flush();
            $this->autoLogin($user);

            return $this->redirectToRoute('file_index');

        }
        else
        {
            $this->redirectToRoute('user_index');
        }

        return $this->render('@App/user/register.html.twig',array('register'=>$registerForm->createView()));

    }

    public function forgetPasswordAction(Request $request)
    {

        $userinfo = $this->get('security.token_storage')->getToken()->getUser();

        $form=new ForgetPassType();
        $form=$this->createForm(ForgetPassType::class);
        $form=$form->handleRequest($request);

        $mail=$form->get('mail')->getData();
        $username=$form->get('username')->getData();

        /**
         * @var Enduser $user
         */
        $user=$this->getDoctrine()->getRepository('AppBundle:Enduser')->findBy(array('username'=>$username,'mail'=>$mail));

        $change=new ChangePassType();
        $change=$this->createForm(ChangePassType::class);
        if($form->isSubmitted()&&$user)
        {
            foreach ($user as $user)
            {
                $user=$user;
            }
            /**
             * UserPassword Constraint i için login olması gerekiyor.
             */
            $this->autoLogin($user);

            return $this->render('AppBundle:user:changepass.html.twig', array('form'=>$change->createView(),'user'=>$userinfo,'id'=>$user->getId()));
        }
        else
        {
            if($form->isSubmitted()&&!$user)
            {
                $this->get('app.message')->alert(Messages::NOT_MATCH_USERNAME_PASSWORD);
            }

        }
        return $this->render('AppBundle:user:forgetpass.html.twig',
            array('form'=>$form->createView())
        );
    }

    public  function changePasswordAction(Request $request)
    {

        $username = $this->getUser()->getUsername();
        // Equal to $user = $this->getUser();

        $user =new Enduser();
        $form = $this->createForm(ChangePassType::class, $user);

        $user = $this->getDoctrine()->getRepository("AppBundle:Enduser")->findOneBy(array("username"=>$username));
        if($user) {
            $form = $form->handleRequest($request);
            $mail = $user->getMail();
            /**
             * Encode Password
             */
            $pass = $form->get('password')->getData();
            $newPass = $form->get('newPassword')->getData();

            $password = $this->encodePass($user, $newPass);

            $isVerify = $this->verifyPass($user, $pass);
            if ($form->isSubmitted() && $form->isValid()) {

                $user->setPassword($password);
                $r = $this->getDoctrine()->getManager();
                $r->persist($user);
                $r->flush();

                if ($user) {
                    /**
                     * Değişen şifre mail ile gönderiliyor.
                     * @var \AppBundle\Services\Email $email
                     *
                     */
                    $email = $this->get('app.mail');
                    $params = array(
                        'subject' => 'Şifre Yenileme',
                        'from' => $mail,
                        'to' => $mail,
                        'message' => 'Yeni Şifreniz: ' . $newPass
                    );

                    if ($email->sentMailManual($params)) {
                        $this->get('app.message')->alert(Messages::CHANGE_PASS_SUCCESS);
                        return $this->redirectToRoute('logout');
                    }
                    $this->get('app.message')->alert(Messages::FAIL_PROCESS);
                    return $this->redirectToRoute('forget_pass');
                }
                $this->get('app.message')->alert(Messages::OLD_PASS_WRONG);
                return $this->redirectToRoute('forget_pass');
            }
        }
    }

    public function settingsAction(Request $request)
    {
        $username = $this->getUser();

        /**
         * @var EnduserPreferences $EnduserRefrence
         * @var Enduser $user
         */
        $EnduserPreference=$this->get('app.enduser_preferences');
        $form=$this->createForm(SettingsType::class);
        /**
         * EnduserPreferences $EnduserPreferences
         */
        $form->get('api_enabled')->setData($EnduserPreference->getPreference(EnduserPreferences::API_ENABLED,$username->getUsername()));
        $form->get('sharing_enabled')->setData($EnduserPreference->getPreference(EnduserPreferences::SHARING_ENABLED,$username->getUsername()));
        $form->get('sharing_notification')->setData($EnduserPreference->getPreference(EnduserPreferences::SHARING_NOTIFICATION_ENABLED,$username->getUsername()));

        $form=$form->handleRequest($request);
        if ($form->isSubmitted()&&$form->isValid())
        {
            $user=$this->getDoctrine()->getRepository('AppBundle:Enduser')->findOneBy(array('username'=>$username->getUsername()));

            $preferences=array( 'sharing_enabled'=>$form->get('sharing_enabled')->getData(),
                                'sharing_notification_enabled'=>$form->get('sharing_notification')->getData(),
                                'api_enabled'=>$form->get('api_enabled')->getData());

            $user->setPreferences($preferences);
            $r=$this->getDoctrine()->getManager();
            $r->persist($user);
            $r->flush();

        }
        return $this->render('@App/user/settings.html.twig',array('user'=>$username,'form'=>$form->createView()));
    }



    protected function encodePass($user,$plainPass)
    {

        /**
         * Şifreyi encode ediyor.
         */

        $encoder = $this->container->get('security.password_encoder');
        return $encoded = $encoder->encodePassword($user, $plainPass);
    }


    protected function verifyPass($user,$plainPass)
    {

        /**
         * @var Enduser $user
         */
        if ($user) {
            foreach ($user as $user) {
                $user = $user;
            }
            /**
             * Şifrenin doğrulanma işlemini gerçekleştiriyor.
             */
            $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
            $isValid = $encoder->isPasswordValid($user->getPassword(), $plainPass, $user->getSalt());
            if ($isValid) {
                return true;

            } else {
                return false;
            }
        }

    }

    protected  function autoLogin($user)
    {
        /**
         * Otomatik Login.
         */
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
    }

}
