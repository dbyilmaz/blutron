<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BatchDownloadRequest;
use AppBundle\Form\CloudFileType;
use AppBundle\Form\ShareType;
use AppBundle\Form\SentType;
use AppBundle\Services\Messages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\CloudFile;
use Symfony\Component\HttpFoundation\Response;

class FilesController extends Controller
{

    public function indexAction(Request $request)
    {

        /*Giriş yapmış kullanıcı bilgilerini çekmeyi sağlar. Bunun dışında kullanıcı rolu vb..*/
        $user = $this->get('security.token_storage')->getToken()->getUser();

        /**
        * @var $form CloudFileType
         */
        $form=$this->createForm(CloudFileType::class);

        $form->handleRequest($request);

        /**
         * @var $form ShareType
         */
        $shareForm=$this->createForm(ShareType::class);

        /**
         * @var $form SentType
         */
        $sentForm=$this->createForm(SentType::class);

        if($form->isSubmitted()&&$form->isValid())
        {
            /*Form Verilerini Çeker*/
            $formData = $form->getData();

            $s3=$this->get('app.s3');
            $status=$s3->putObject($formData,$user);

            if ($status)
            {
                $this->get('app.message')->alert(Messages::FILE_UPLOAD_SUCCESS);
                return $this->redirectToRoute('file_index');
            }
            else
            {
                return $this->render('AppBundle:exception:repeat.html.twig', array("hata"=>Messages::FILE_UPLOAD_FAIL));
            }

        }

        /*Listeleme Kısmı*/
        $files=$this->getDoctrine()->getRepository('AppBundle:CloudFile')
            ->findBy(
                array('user'         =>  $user),
                array('fav'=>'DESC','uploadedAt' => 'DESC')
            );

        $allFi=0;

        /** @var CloudFile $files*/
        foreach ($files as $file) {
            $allFi=$allFi+($file->getFileSize()) ;

        }
        $sumFileSize=round(($allFi),5);
        $countFiles=count($files);


        // replace this example code with whatever you need
        return $this->render('AppBundle::files/index.html.twig',array(  'form'          =>$form->createView(),
                                                        'files'         =>$files,
                                                        'sumFileSize'   =>$sumFileSize,
                                                        'countFiles'    =>$countFiles,
                                                        'user'          =>$user,
                                                        'share'         =>$shareForm->createView(),
                                                        'sent'          =>$sentForm->createView(),
            ));
    }


    public function deleteAction($id)
    {
        /*Giriş yapmış kullanıcı bilgilerini çekmeyi sağlar. Bunun dışında kullanıcı rolu vb..*/
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $s3=$this->get('app.s3');
        $status=$s3->deleteObject($id,$user);

        if ($status)
        {
            $this->get('app.message')->alert(Messages::FILE_DELETE_SUCCESS);
            return $this->redirectToRoute('file_index');
        }
        else
        {
            return new JsonResponse([],404);
        }

    }


    public function  downloadAction($fileId)
    {
        /*Giriş yapmış kullanıcı bilgilerini çekmeyi sağlar. Bunun dışında kullanıcı rolu vb..*/
        $user = $this->get('security.token_storage')->getToken()->getUser();

        $files=$this->getDoctrine()->getRepository('AppBundle:CloudFile');
        $file=$files->findOneBy(array('id'=>$fileId,'user'=>$user));

        /*Dosyayı indirmeye çalışan ile dosya sahibinin id leri aynımı?*/
        if($file) {
            /*Dosya İndirme Bağlantıları Buraya Gelecek*/
            return $this->render('AppBundle:files:down.html.twig');
        }else
        {
            return $this->render('AppBundle:exception:404.html.twig');
        }

    }


    public  function  favAction(Request $request)
    {
        /*Dosya id sini viewden çeker.*/
        $id= $request->getContent();

        $r=$this->getDoctrine()->getManager();
        $files=$this->getDoctrine()->getRepository('AppBundle:CloudFile')->find($id);

        /*Veritabanından favori durumunun kontrolü yapıyor.*/
        if($files->getFav())
        {
            $files->setFav(false);
        }
        else
        {
            $files->setFav(true);
        }

        $r=$this->getDoctrine()->getManager();
        $r->persist($files);
        $r->flush();

        return new Response('OK');

    }


    public function batchDownloadAction(Request $request)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        $files = $request->get("batchF");
        if (isset($files)&&trim($files)!="")
        {
            /*
             * @var BatchDownloadRequest $batchDown
             */
            $batchDown=new BatchDownloadRequest();
            $batchDown->setOwner($user);
            $batchDown->setFiles(explode(',',$files));
            $r=$this->getDoctrine()->getManager();
            $r->persist($batchDown);
            $r->flush();
            $this->get('app.message')->alert(Messages::FILE_PREPARE_DOWNLOAD);
        }
        else
        {
            $this->get('app.message')->alert(Messages::FILE_SELECT_MIN_ONE);
        }
        return $this->redirectToRoute('file_index');
    }
}
