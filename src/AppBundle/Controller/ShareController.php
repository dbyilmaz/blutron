<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Enduser;
use AppBundle\Entity\SharedCloudFile;
use AppBundle\Form\ShareType;
use AppBundle\Form\SentType;
use AppBundle\Services\EnduserPreferences;
use AppBundle\Services\Messages;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
class ShareController extends Controller
{

    public  function shareAction(Request $request)
    {
        /**
         * @var $form ShareType
         */
        $shareForm=$this->createForm(ShareType::class);
        $shareForm->handleRequest($request);
        $data=$shareForm->getData();

        $mail=trim($data['mail']);
        $shareFileId=(int)$data['fileId'];


        $r=$this->getDoctrine()->getManager();
        /*
         * @var Enduser $mailStatus
         */
        $targetUser=$this->getDoctrine()->getRepository('AppBundle:Enduser')->findOneBy(array('mail'=>$mail));


        if(isset($mail)&&$mail!="") {

            if ($shareForm->isValid()) {

                if($targetUser) {

                    $sharingEnable=$this->get('app.enduser_preferences')->getPreference(EnduserPreferences::SHARING_ENABLED,$targetUser->getUsername());

                    if ($sharingEnable) {

                        if ($shareForm->isSubmitted()) {

                            $file = $this->getDoctrine()->getRepository('AppBundle:CloudFile')->findOneBy(array('id' => $shareFileId));
                            $owner = $file->getUser();
                            $fileName = $file->getUniqName();
                            $shareOwnerId = $owner->getId();
                            $ownerName = $owner->getName();
                            $ownerMail = $owner->getMail();
                            $link = sprintf("blutron.yt/p/%s/%s", $shareFileId, $fileName);

                            /*
                             * ses mail gönderme.
                             */
                            $status=$this->get('app.s3')->sentMail($ownerName, $link, $mail, $ownerMail);
                            if ($status)
                            {
                                $this->get('app.message')->alert(Messages::MAIL_SEND_SUCCESS);
                                $this->redirectToRoute('file_index');

                            }
                            else
                            {
                                $this->get('app.message')->alert(Messages::MAIL_SEND_FAIL);
                                $this->redirectToRoute('file_index');

                            }
                            /*
                             * @var $shareFile SharedCloudFile
                             */
                            $shareFile = new SharedCloudFile();
                            $shareFile->setFile($file);
                            $shareFile->setOwner($owner);
                            $shareFile->setTarget($targetUser);
                            $r = $this->getDoctrine()->getManager();
                            $r->persist($shareFile);
                            $r->flush();

                            return $this->redirectToRoute('file_index');
                        }
                        $this->get('app.message')->alert(Messages::FAIL_PROCESS);
                        return $this->redirectToRoute('file_index');
                    }
                    $this->get('app.message')->alert(Messages::MAIL_NOT_ENABLE_SHARING);
                    return $this->redirectToRoute('file_index');
                }
                $this->get('app.message')->alert(Messages::NOT_FOUND_BLUTRON_MAIL);
                return $this->redirectToRoute('file_index');
            }
            $this->get('app.message')->alert(Messages::INVALID_MAIL);
            return $this->redirectToRoute('file_index');
        }
        $this->get('app.message')->alert(Messages::BLANK_MAIL);
        return $this->redirectToRoute('file_index');

    }


    /*Gonder Action*/
    public  function sentAction(Request $request)
    {
        /**
         * @var $form ShareType
         */
        $shareForm = $this->createForm(SentType::class);
        $shareForm->handleRequest($request);
        $data = $shareForm->getData();

        $mail = trim($data['email']);
        $shareFileId = (int)$data['sentFileId'];

        $r = $this->getDoctrine()->getManager();
        /*
         * @var $mailStatus Enduser
         */
        $targetUser = $this->getDoctrine()->getRepository('AppBundle:Enduser')->findOneBy(array('mail' => $mail));

        if (isset($mail) && $mail != "") {
            if ($shareForm->isValid()) {
                if ($targetUser) {

                    $sharingEnable=$this->get('app.enduser_preferences')->getPreference(EnduserPreferences::SHARING_ENABLED,$targetUser->getUsername());

                    if ($shareForm->isSubmitted()) {
                        if ($sharingEnable) {
                            $file = $this->getDoctrine()->getRepository('AppBundle:CloudFile')->findOneBy(array('id' => $shareFileId));
                            $owner = $file->getUser();
                            $fileName = $file->getUniqName();
                            $ownerMail = $owner->getMail();

                            /** @var \AppBundle\Services\Email $email */
                            $email = $this->get('app.mail');
                            $params = array(
                                'subject' => $file->getFileName(),
                                'from' => $ownerMail,
                                'to' => $mail,
                                'cc' => $ownerMail,
                                'reply' => $ownerMail,
                                'attachment' => $fileName,
                                'message' => $fileName
                            );

                            #pending maile gidiyor.
                            $email->addMail($params);

                            $this->get('app.message')->alert(Messages::MAIL_SEND_SUCCESS);
                            return $this->redirectToRoute('file_index');
                        }
                        $this->get('app.message')->alert(Messages::MAIL_NOT_ENABLE_SHARING);
                        return $this->redirectToRoute('file_index');
                    }
                    $this->get('app.message')->alert(Messages::FAIL_PROCESS);
                    return $this->redirectToRoute('file_index');
                }
                $this->get('app.message')->alert(Messages::NOT_FOUND_BLUTRON_MAIL);
                return $this->redirectToRoute('file_index');
            }
            $this->get('app.message')->alert(Messages::INVALID_MAIL);
            return $this->redirectToRoute('file_index');
        }
        $this->get('app.message')->alert(Messages::BLANK_MAIL);
        return $this->redirectToRoute('file_index');
    }

    /*Giriş yapmadan dosya indirmeyi sağlar.*/
    public function downloadAction($fileId,$fileName)
    {

        $em=$this->getDoctrine()->getManager();
        $file=$em->getRepository('AppBundle:SharedCloudFile')->findOneBy(array('file'=>$fileId));
        $ownerMail=$file->getOwner()->getMail();

        /**
         * @var EnduserPreferences $enduserPreferences
         * Dosya ilk indirildiğinde gönderilecek mail durumunu komtrol eder.
         */

        $enableNotification=$this->get('app.enduser_preferences')->getPreference(EnduserPreferences::SHARING_NOTIFICATION_ENABLED,$file->getOwner()->getUsername());

        if (!$file) {
            $this->get('app.message')->alert(Messages::MAIL_SEND_FAIL);
            $this->redirectToRoute('file_index');
        }

        /*Her indirmede FileDown'u 1 Arttırır.*/
        $fileDown=$file->getFileDown();
        if($fileDown==0&&$enableNotification)
        {
            /*
             * Bildirim maili gönderme
             */
            $this->get('app.s3')->notificationSentMail($fileName,$ownerMail);
        }
        $file->setFileDown($fileDown+1);
        $em->flush();
        $link=$this->get('app.s3')->generateLink($fileName);
        return $this->redirect(sprintf("%s://%s%s?%s",$link->getScheme(),$link->getHost(),$link->getPath(),$link->getQuery()));

    }


}
