<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 29.04.2017
 * Time: 15:06
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class ShareType extends  AbstractType
{


    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('mail',EmailType::class,array('attr'=>array('class'=>'form-control'),
        'constraints' => array(
            new NotBlank(),
            new Email()),
        ))
            ->add('fileId',HiddenType::class,array('attr'=>array('id'=>'pFilId')));
    }


    public function configureOptions(OptionsResolver $resolver)
    {

    }





}