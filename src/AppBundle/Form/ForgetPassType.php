<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ForgetPassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mail',EmailType::class,array('label'=>'Mail Adresi','attr'=>array('class'=>'form-control')))
            ->add('username',TextType::class,array('label'=>'Kullanıcı Adı','attr'=>array('class'=>'form-control')));

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }

}
