<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 27.04.2017
 * Time: 23:15
 */

namespace AppBundle\Form;



use AppBundle\Entity\Enduser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserLoginType extends  AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('username',TextType::class, array('label'=>'Kullanıcı Adı','attr'=>array('class'=>'form-control')))
        ->add('password',PasswordType::class, array('label'=>'Şifre','attr'=>array('class'=>'form-control')));
    }


    public function configureOptions(OptionsResolver $resolver)
    {

    }

}