<?php

namespace AppBundle\Form;

use AppBundle\Entity\Enduser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;

class ChangePassType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('password',PasswordType::class,array('label'=>'Eski Şifre','attr'=>array('class'=>'form-control'),
            'constraints' => new UserPassword(),
        ))
            ->add('newPassword',PasswordType::class,array('label'=>'Yeni Şifre','attr'=>array('class'=>'form-control') ,'mapped' => false,
            ))
            ->add('userid',HiddenType::class,array('mapped'=>false
            ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Enduser::class,
        ));
    }

}
