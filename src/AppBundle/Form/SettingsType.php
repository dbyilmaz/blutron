<?php

namespace AppBundle\Form;

use AppBundle\Entity\Enduser;
use AppBundle\Services\EnduserPreferences;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class SettingsType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sharing_enabled',ChoiceType::class,
            array('label'=>'Başka kullanıcıların benimle dosya paylaşmasına',
                'choices' => array(
                    'İzin Veriyorum' => '1',
                    'İzin Vermiyorum' => '0'
                ),
                'multiple' => false,
                'expanded' => true,
                'required' => true,
            ))

                ->add('sharing_notification',ChoiceType::class,
                    array('label'=>'Paylaştığım dosyalara erişildiğinde email ile bilgilendirilmek',
                        'choices' => array(
                            'İstiyorum' => '1',
                            'İstemiyorum' => '0'
                        ),
                        'multiple' => false,
                        'expanded' => true,
                        'required' => true,
                    ))

                ->add('api_enabled',ChoiceType::class,
                    array('label'=>'Dosyalarıma ait api ile erişime',
                        'choices' => array(
                            'İzin Veriyorum' => '1',
                            'İzin Vermiyorum' => '0'
                        ),
                        'multiple' => false,
                        'expanded' => true,
                        'required' => true,
                    ));

    }

    public function configureOptions(OptionsResolver $resolver)
    {

    }




}
