<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 06.05.2017
 * Time: 10:00
 */

namespace AppBundle\Form;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;
class SentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('email',EmailType::class,array('label'=>'Email Adresi','attr'=>array('class'=>'form-control'),
            'constraints' => array(
                new NotBlank(),
                new Email()),
        ))
            ->add('sentFileId',HiddenType::class,array());
    }

}