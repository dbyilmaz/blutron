<?php

namespace AppBundle\Form;

use AppBundle\Entity\CloudFile;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CloudFileType extends AbstractType
{

    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('file',FileType::class,array('label'=>'Dosya','attr'=>array('class'=>'form-control')));
    }
    

    public function configureOptions(OptionsResolver $resolver)
    {

    }




}
