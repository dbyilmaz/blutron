<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 11.05.2017
 * Time: 10:11
 */

namespace AppBundle\Form;


use AppBundle\Entity\Enduser;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Expression;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class UserRegisterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

       $form= $builder
            ->add('name',TextType::class,array('label'=>'Adı Soyadı','attr'=>array('class'=>'form-control'),
                'constraints'=>
                    array(
                        new Length(array('min' => 1,'minMessage'=>'En az 1 Karakter Girmelisiniz.')),
                        new NotBlank(array('message'=>'Ad Soyad Kısmını Boş Bırakamassınız.')))
            ))

            ->add('mail',TextType::class,array('label'=>'Email Adresi','attr'=>array('class'=>'form-control'),
                'constraints'=>
                    array(
                        new Email(array('message'=>'Geçerli Bir Email Adresi Giriniz.')),
                        new NotBlank(array('message'=>'Email Kısmını Boş Bırakamassınız.')))
                ))

            ->add('username',TextType::class,array('label'=>'Kullanıcı Adı','attr'=>array('class'=>'form-control'),
                'constraints'=>
                    array(
                        new NotBlank(array('message'=>'Kullanıcı Adı Kısmını Boş Bırakamassınız.')))


            ))

            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Girdiğiniz şifreler aynı olmalı.',
                'options' => array('attr' => array('class' => 'form-control')),
                'required' => true,
                'first_options'  => array('label' => 'Şifre','error_bubbling' => true),
                'second_options' => array('label' => 'Şifre (Yeniden)'),
                'constraints'=>new NotBlank(array('message'=>'Şifre Kısmını Boş Bırakamazsınız'))
            ))


            ->add('city',ChoiceType::class,array('label'=>'Şehir','choices'=>$this->csvReader(),'attr'=>array('class'=>'form-control')))


            ->add('sex',ChoiceType::class,
                array('label'=>'Cinsiyet',
                    'choices' => array(
                        'Erkek' => true,
                        'Kadın' => false
                    ),
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
                    'data'     => true,
                    'constraints'=>new NotBlank(array('message'=>'Cinsiyet Kısmını Boş Bırakamassın'))
                ))


            ->add('maidenName',TextType::class,array('label'=>'Evlenemeden Önceki Soyadı','attr'=>array('class'=>'form-control'),'required'=>false));


        $builder->addEventListener(FormEvents::PRE_SUBMIT,function (FormEvent $event)
        {
            $user=$event->getData();
            $form=$event->getForm();

            if(!$user)
            {
                return;

            }
            if ($user['sex']==1)
            {
                $form->add('maidenName',TextType::class,array('label'=>'Evlenmeden Önceki Soyadı','attr'=>array('class'=>'form-control'),'required'=>false));
            }
            else
            {
                $form->add('maidenName',TextType::class,
                    array(
                        'label'=>'Evlenmeden Önceki Soyadı',
                        'attr'=>array('class'=>'form-control'),
                        'required'=>true,
                        'constraints'=>new NotBlank(array('message'=>'Evlenmeden Önceki Soyadı Kısmını Boş Bırakamazsınız.'))
                        ));
            }
        });



    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Enduser::class,
        ));
    }



    protected function  csvReader()
    {


        $url = "https://commondatastorage.googleapis.com/ckannet-storage/2012-07-19T130158/cities-of-Turkey---Sheet1.csv";
        $csvData = file_get_contents($url);
        $lines = explode(PHP_EOL, $csvData);
        $array = array();

        foreach ($lines as $line => $value) {
            $array[] = str_getcsv($value);
        }

        $il=array();
        $plaka=array();

        for ($i=1;$i<count($array);$i++)
        {
            if(isset($array[$i][2]))
            {
                $il[]=$array[$i][2];
                $plaka[]=$array[$i][0];
            }
        }

        $ilData=array_combine($il,$plaka);

        return $ilData;
    }

}