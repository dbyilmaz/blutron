<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 13.05.2017
 * Time: 13:20
 */

namespace AppBundle\Services;


use AppBundle\Entity\Enduser;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class EnduserPreferences
{

    const SHARING_ENABLED = 'sharing_enabled';
    const SHARING_NOTIFICATION_ENABLED = 'sharing_notification_enabled';
    const API_ENABLED = 'api_enabled';

    protected $preferences = array();

    protected $sessionUsername;

    protected $em;


    /**
     * EnduserPreferences constructor.
     * @param Enduser $user
     * @param EntityManager $em
     */
    function __construct($user, EntityManager $em)
    {
            $this->sessionUsername = $user->getUsername();
            $this->registerUserPreferences($user);
            $this->em = $em;
    }


    public function getPreference($key, $username = null)
    {
        if (is_null($username)) {
            $username = $this->sessionUsername;
        }
        if (!isset($this->preferences[$username])) {
            $user = $this->em->getRepository('AppBundle:Enduser')->findOneBy(array('username' => $username));
            $this->registerUserPreferences($user);

        }
        return ($this->preferences[$username][$key]);
    }


    /**
     * @param $user Enduser
     */
    protected function registerUserPreferences($user)
    {
        $this->preferences[$user->getUsername()] = $user->getPreferences();


    }

}