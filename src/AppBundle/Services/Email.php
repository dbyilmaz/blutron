<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 05.05.2017
 * Time: 21:54
 */

namespace AppBundle\Services;
use AppBundle\Entity\PendingEmail;
use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \PHPMailer ;
use \SMTP;


class Email
{
    const STATUS_ADDED = 0;
    const STATUS_SENT = 1;
    const STATUS_FAILED = -1;

    const MAIL_SEPARATOR = ';';


    /**
     * @var EntityManager $em;
     */
    protected $em;

    protected $cont;


    function __construct(EntityManager $em,ContainerInterface $container)
    {
        $this->em=$em;
        $this->cont=$container;
    }


    public function  addMail(array $params)
    {
        $mail=new PendingEmail();
        $mail->setSubject($params['subject']);
        $mail->setFromEmail($params['from']);

        $mail->setToEmail($params['to']);

        if(isset($params['bcc']))
        {
            $mail->setBccEmail(implode(self::MAIL_SEPARATOR,$params['bcc']));
        }

        if(isset($params['cc']))
        {
            $mail->setCcEmail($params['cc']);
        }

        $mail->setReplyToEmail($params['from']);
        $mail->setBody($params['message']);
        $mail->setAttachment($params['attachment']);
        $mail->setStatus(self::STATUS_ADDED);

        $this->em->persist($mail);
        $this->em->flush();
    }





    public function sendMail()
    {

        $email=new \PHPMailer();
        $email->IsSMTP();
        $email->SMTPAuth=true;
        $email->CharSet = 'UTF-8';
        $email->Host=$this->cont->getParameter('mailer_host');
        $email->Port=$this->cont->getParameter('mailer_port');
        $email->SMTPSecure=$this->cont->getParameter('mailer_encrypt');
        $email->Username=$this->cont->getParameter('mailer_user');
        $email->Password=$this->cont->getParameter('mailer_password');


        /** @var \AppBundle\Entity\PendingEmail $emails */
        $emails=$this->em->getRepository('AppBundle:PendingEmail')->findBy(array('status'=>0));

        /** @var \AppBundle\Entity\PendingEmail $e */
        foreach ($emails as $e)
        {

                /*Gönderen Kişi*/
                $email->setFrom($e->getFromEmail());

                /*Alıcı*/
                $toAdresses=explode(self::MAIL_SEPARATOR,$e->getToEmail());
                foreach ($toAdresses as $to)
                {
                    $email->addAddress($to);
                }

                /*Cevap Mail*/
                $email->addReplyTo($e->getFromEmail());


                /*cc*/
                $toCc=explode(self::MAIL_SEPARATOR,$e->getCcEmail());
                foreach ($toCc as $cc)
                {
                    $email->addCC($cc);
                }



                /*Mail Ekleri*/
                $attachments=explode(self::MAIL_SEPARATOR,$e->getAttachment());
                foreach ($attachments as $att)
                {
                    $link = $this->getLink($att);
                    $link=sprintf("%s://%s%s?%s",$link->getScheme(),$link->getHost(),$link->getPath(),$link->getQuery());
                    $email->addStringAttachment(file_get_contents($link),$att);
                }

                /*İçerik Html*/
                $email->isHTML(true);


                /*Konu*/
                $email->Subject=$e->getSubject();

                /*İçerik*/
                $email->Body=$e->getBody();


                if($email->send())
                {
                    $e->setStatus('1');
                    $e->setSentAt(new\DateTime('now'));
                }
                else
                {
                    $e->setStatus('-1');

                }

                $this->em->persist($e);
                $this->em->flush();
        }


    }

    public  function getLink($fileName)
    {

        try {
            $t = new S3Client([
                'version' => 'latest',
                'signature' => "v4",
                'region' => "ap-south-1",
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,


            ]);


            /*Geçici dosya indirme*/
            $expireTime = 1;
            $cmd = $t->getCommand('GetObject', [
                'Bucket' => $this->cont->getParameter('aws_bucket'),
                'Key' => 'upload/' . $fileName,
                'signature' => 'v4',
                'region' => 'ap-south-1',
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,
            ]);

            $tmpUrl = $t->createPresignedRequest($cmd, '+' . $expireTime . ' minutes');
            $tmpUrl = $tmpUrl->getUri();
            return ($tmpUrl);

        }catch (\S3Exception $e){

        }

    }



    public function sentMailManual($params)
    {
        $email=new \PHPMailer();
        $email->IsSMTP();
        $email->SMTPAuth=true;
        $email->CharSet = 'UTF-8';
        $email->Host=$this->cont->getParameter('mailer_host');
        $email->Port=$this->cont->getParameter('mailer_port');
        $email->SMTPSecure=$this->cont->getParameter('mailer_encrypt');
        $email->Username=$this->cont->getParameter('mailer_user');
        $email->Password=$this->cont->getParameter('mailer_password');

        $email->setFrom($params['from']);
        $email->addAddress($params['to']);
        $email->Subject=$params['subject'];
        $email->Body=$params['message'];
        $email->addReplyTo=$params['from'];
        return $email->send();
    }


}