<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 22.05.2017
 * Time: 16:29
 */

namespace AppBundle\Services;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;

class Messages
{
    #Mail ile ilgili işlemler
    const INVALID_MAIL="Geçerli bir mail giriniz.";
    const NOT_FOUND_BLUTRON_MAIL="Bu mail Blutronda bulunmuyor. Lütfen Blutronda bulunan mail adresi giriniz.";
    const MAIL_NOT_ENABLE_SHARING="Bu mail paylaşıma kapalıdır.";
    const MAIL_SEND_FAIL="Mail Gönderilemedi.";
    const MAIL_SEND_SUCCESS="Mail Başarılı Bir Şekilde Gönderildi.";
    const BLANK_MAIL="Mail Kısmını Boş Bırakmayınız.";
    const FAIL_PROCESS="İşleminiz başarısız oldu tekrar deneyiniz.";

    #Dosya İşlemleri
    const FILE_UPLOAD_SUCCESS="Dosya Başarıyla Yüklendi.";
    const FILE_UPLOAD_FAIL="Dosya Yükelnirken Bir Hata Meydana Geldi İşleminizi Tekrar Yapınız :(";
    const FILE_PREPARE_DOWNLOAD="Dosyalarınız İndirmek İçin Hazırlanıyor. Hazır Olduğunda Mail ile Bildirim Alıcaksınız.";
    const FILE_SELECT_MIN_ONE="İndirmek için en az 1 dosya seçmeniz gerekiyor.";
    const FILE_DELETE_SUCCESS="Dosyası Başarıyla Silindi";

    #Kullanıcı işlemleri
    const NOT_MATCH_USERNAME_PASSWORD="Kullanıcı Adı ve Mail Eşleşmiyor.";
    const CHANGE_PASS_SUCCESS="Şifreniz Başarılı Bir Şekilde Değiştirildi ve Mailinize Gönderildi.";
    const OLD_PASS_WRONG="Eski şifrenizi yanlış girdiniz.";


    private $session;

    public function __construct( Session $session)
    {
        $this->session = $session;
    }
    public function alert($message)
    {
        $this->session->getFlashBag()->add('notice', $message);

    }

}