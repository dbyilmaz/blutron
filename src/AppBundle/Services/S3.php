<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 22.05.2017
 * Time: 14:25
 */

namespace AppBundle\Services;


use AppBundle\Entity\CloudFile;
use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;
use Aws\Ses\SesClient;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Tests\Compiler\C;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;

class S3
{
    protected $cont;
    protected $em;

    function __construct(ContainerInterface $container,EntityManager $entityManager)
    {
        $this->cont=$container;
        $this->em=$entityManager;
    }


    public function putObject($formData,$user)
    {

        /** @var UploadedFile $file */
        $file = $formData['file'];
        /** @var UploadedFile $file */
        $fileName=$file->getClientOriginalName();
        $temp_name=$file->getPathname();
        $extension=$file->getClientOriginalExtension();
        $size=$file->getClientSize();
        $file=$file->move($this->cont->get('kernel')->getRootDir().'/../tmp',$fileName);
        chmod($file->getPathname(),0777);
        $filenameUniq=uniqid().$fileName;

        $s3=$this->getS3();
        /*Bucket içindeki upload dizinine yüklüyor*/
        $s3=$s3->putObject([
            'Bucket'=>$this->cont->getParameter('aws_bucket'),
            'Key'=>sprintf('upload/%s',$filenameUniq),
            'Body'=>file_get_contents($file->getPathname()),
            'ACL'=>'',
        ]);

        /**
         * @var CloudFile $uploadFile
         */

        $uploadFile=new CloudFile();

        $fileSize=($size);
        $uploadFile->setFileName($fileName);
        $uploadFile->setFileSize(($fileSize/1024));
        $uploadFile->setUser($user);
        $uploadFile->setUniqName($filenameUniq);
        $uploadFile->setFav(false);
        $this->em->persist($uploadFile);
        $this->em->flush();
        $delPath=sprintf($this->cont->get('kernel')->getRootDir().'/../tmp/%s',$fileName);
        unlink($delPath);

        if ($s3)
        {
            return true;
        }
        else
        {
            return false;
        }

    }

    public function deleteObject($id,$user)
    {


        $files = $this->em->getRepository('AppBundle:CloudFile');
        $file = $files->findOneBy(array('id' => $id, 'user' => $user));


        if ($file) {

            /** @var CloudFile $file */
            $s3File = $file->getUniqName();

            $s3 = $this->getS3();

            $s3 = $s3->deleteObject(['Bucket' => $this->cont->getParameter('aws_bucket'), 'Key' => sprintf('upload/%s', $s3File)]);
            if ($s3) {
                $this->em->remove($file);
                $this->em->flush();

                return true;
            } else {
                return false;
            }
        }
    }





    public function listObject()
    {
        try {
            $t = new S3Client([
                'version' => 'latest',
                'signature' => "v4",
                'region' => "ap-south-1",
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,


            ]);

            $objects=$t->getIterator('ListObjects',[
                'Bucket'=>$this->cont->getParameter('aws_bucket'),
                'Prefix' => 'uploads/'
            ]);

            $url=$t->getBucketLocation(['Bucket'=>$this->cont->getParameter('aws_bucket')]);
            # $sil=$t->deleteObject(['Bucket'=>$this->getParameter('aws_bucket'),'Key'=>'upload/rootkey.csv']);
        }catch (S3Exception $e)
        {
            return false;
        }
        return true;
    }

    public function generateLink($fileName)
    {
        try {
            $t = new S3Client([
                'version' => 'latest',
                'signature' => "v4",
                'region' => "ap-south-1",
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,


            ]);


            /*Geçici dosya indirme*/
            $expireTime = 1;
            $cmd = $t->getCommand('GetObject', [
                'Bucket' => $this->cont->getParameter('aws_bucket'),
                'Key' => 'upload/' . $fileName,
                'signature' => 'v4',
                'region' => 'ap-south-1',
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,
            ]);

            $tmpUrl = $t->createPresignedRequest($cmd, '+' . $expireTime . ' minutes');
            $tmpUrl = $tmpUrl->getUri();
            return ($tmpUrl);

        }catch (\S3Exception $e){
            return false;
        }

    }


    public function sentMail($ownerName,$link,$mail,$ownerMail)
    {
        $ses=$this->getSES();
        $request = array();
        $request['Source'] = $ownerMail;
        $request['Destination']['ToAddresses'] = array($mail);
        $request['Message']['Subject']['Data'] = "Blutron Dosya Paylaşımı";
        $request['Message']['Body']['Html']['Data'] = sprintf("<b>%s</b><br> <a href='%s'>Dosya Link</a>",$ownerName,$link);

        try {
            $result = $ses->sendEmail($request);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function notificationSentMail($ownerMail,$fileName)
    {
        $ses=$this->getSES();

        $request = array();
        $request['Source'] = $ownerMail;
        $request['Destination']['ToAddresses'] = array($ownerMail);
        $request['Message']['Subject']['Data'] = "Dosya Bildirimi";
        $request['Message']['Body']['Html']['Data'] = sprintf("<b>%s</b> Adındaki Dosya İlkdefa İndirildi.",$fileName);

        try {
            $result = $ses->sendEmail($request);
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    protected function getS3()
    {
        try{

            $s3 = new S3Client([
                'version' => 'latest',
                'signature'=> "v4",
                'region'=>"ap-south-1",
                'credentials' => array(
                    'key'=>$this->cont->getParameter('aws_key'),
                    'secret'=>$this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport'=>false,


            ]);

            return $s3;

        }catch (\S3Exception $exception)
        {
            return false;
        }

    }


    protected function getSES()
    {


        $client = new SesClient([
            'version' => 'latest',
            'signature'=> "v4",
            'region'=>"us-west-2",
            'credentials' => array(
                'key'=>$this->cont->getParameter('aws_key'),
                'secret'=>$this->cont->getParameter('aws_secret')
            ),


        ]);

        return $client;
    }

}