<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 06.05.2017
 * Time: 15:05
 */

namespace AppBundle\Services;


use AppBundle\Entity\CloudFile;
use Aws\S3\S3Client;
use Doctrine\ORM\EntityManager;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\DependencyInjection\ContainerInterface;
use \ZipArchive as Zip;
use \PHPMailer;
use \SMTP;

class FileZip
{
    const STATUS_ADDED = 0;
    const STATUS_SENT = 1;
    const STATUS_FAILED = -1;

    const MAIL_SEPERATOR = ';';


    /**
     * @var EntityManager $em;
     */
    protected $em;

    /**@var ContainerInterface $cont
     *
     */
    protected $cont;



    function __construct(EntityManager $em,ContainerInterface $container)
    {
        $this->em=$em;
        $this->cont=$container;
    }


    /***
     * Veritabanından durumu 0 olanları çeker ve indirme fonksiyonuna yollar.
     */
    public function sendFiles()
    {
        /** @var \AppBundle\Entity\BatchDownloadRequest $batchRequest */
        $batchRequest=$this->em->getRepository('AppBundle:BatchDownloadRequest')->findBy(array('status'=>0));
        foreach ($batchRequest as $batch)
        {
            $this->fileDown($batch);
        }

    }


    /**
     * Dosyaları indirir ver fileZip fonksiyonuna yollar.
     */
    protected function fileDown($batchRequestId)
    {

        /** @var \AppBundle\Entity\BatchDownloadRequest $batchRequest */
        $batchRequest=$this->em->getRepository('AppBundle:BatchDownloadRequest')->findOneBy(array('id'=>$batchRequestId));

        $owner=$batchRequest->getOwner();
        $ownerMail=$owner->getMail();
        if ($batchRequest)
        {
            $files=$batchRequest->getFiles();
            $cloudFiles = $this->em->getRepository('AppBundle:CloudFile')->findBy(array('id' => $files));
            $uniqueNames = [];
            if(count($cloudFiles))
            {
                /** @var CloudFile $cloudFile */
                foreach ($cloudFiles as $cloudFile)
                {
                    $uniqueNames[] = $cloudFile->getUniqName();

                }

                for($i=0;$i<count($uniqueNames);$i++)
                {
                    /**
                     * Aws linki oluşturur dosyayı lokale indirir.
                     */
                    $expire=1;
                    $dir="upload";
                    $bucketName=$this->cont->getParameter('aws_bucket');
                    $fileLink=$this->getDownLink($uniqueNames[$i],$expire,$bucketName,$dir);
                    $downFile=file_get_contents($fileLink);
                    $error =[];
                    if ($downFile)
                    {
                        $batchRequest->setStatus(1);
                        $error[]="hatasız";
                    }
                    else
                    {
                        $error=$uniqueNames[$i];

                        $batchRequest->setStatus(0);
                        $this->em->persist($batchRequest);
                        $this->em->flush();
                        break;
                    }
                    $error=implode(';',$error);
                    $batchRequest->setSentAt(new\DateTime('now'));
                    $batchRequest->setError($error);
                    $this->em->persist($batchRequest);
                    $this->em->flush();

                    file_put_contents($this->cont->get('kernel')->getRootDir().'/../tmp/batchfiles/files/'.$uniqueNames[$i],$downFile);
                    chmod($this->cont->get('kernel')->getRootDir().'/../tmp/batchfiles/files/'.$uniqueNames[$i],0777);
                }

                /**
                 * İndirilen dosyaların isimlerini ziplenmesi için gönderir.
                 */
                $this->fileZip($uniqueNames,$ownerMail);

            }



        }

    }

    protected function fileZip($uniqueNames,$ownerMail)
    {

        $zip=new Zip();
        $time = date('now').uniqid();
        $zipName=sprintf('%s.zip',$time);
        $zipPath=$this->cont->get('kernel')->getRootDir().'/../tmp/batchfiles/zips/'.$time.'.zip';
        $zip->open($zipPath,Zip::CREATE);
        for($i=0;$i<count($uniqueNames);$i++)
        {
            $zip->addFile($this->cont->get('kernel')->getRootDir().'/../tmp/batchfiles/files/'.$uniqueNames[$i],$uniqueNames[$i]);
        }

        $zip->close();
        chmod($zipPath,0777);
        for($i=0;$i<count($uniqueNames);$i++)
        {
            unlink($this->cont->get('kernel')->getRootDir().'/../tmp/batchfiles/files/'.$uniqueNames[$i]);
        }
        $this->zipUpload($zipPath,$zipName,$ownerMail);



    }

    protected function zipUpload($zipPath,$zipName,$ownerMail)
    {
        $t = new S3Client([
            'version' => 'latest',
            'signature'=> "v4",
            'region'=>"ap-south-1",
            'credentials' => array(
                'key'=>$this->cont->getParameter('aws_key'),
                'secret'=>$this->cont->getParameter('aws_secret')
            ),
            'aws:SecureTransport'=>false,
        ]);



        /*Bucket içindeki upload dizinine yüklüyor*/
        $t->putObject([
            'Bucket'=>$this->cont->getParameter('aws_bucketzip'),
            'Key'=>sprintf('zips/%s',$zipName),
            'Body'=>file_get_contents($zipPath),
            'ACL'=>'',
        ]);

        $expire=10080;
        $dir="zips";
        $bucketName=$this->cont->getParameter('aws_bucketzip');
        $zipLink=$this->getDownLink($zipName,$expire,$bucketName,$dir);
        $this->sendMail($ownerMail,$zipLink);
        unlink($zipPath);




    }


    protected function getDownLink($fileName,$expire,$bucketName,$dir)
    {
        try {
            $t = new S3Client([
                'version' => 'latest',
                'signature' => "v4",
                'region' => "ap-south-1",
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,


            ]);


            /*Geçici dosya indirme*/
            $cmd = $t->getCommand('GetObject', [
                'Bucket' => $bucketName,
                'Key' => sprintf('%s/%s',$dir,$fileName),
                'signature' => 'v4',
                'region' => 'ap-south-1',
                'credentials' => array(
                    'key' => $this->cont->getParameter('aws_key'),
                    'secret' => $this->cont->getParameter('aws_secret')
                ),
                'aws:SecureTransport' => false,
            ]);

            $tmpUrl = $t->createPresignedRequest($cmd, '+' . $expire . ' minutes');
            $tmpUrl = $tmpUrl->getUri();

            return (sprintf("%s://%s%s?%s",$tmpUrl->getScheme(),$tmpUrl->getHost(),$tmpUrl->getPath(),$tmpUrl->getQuery()));

        }catch (\S3Exception $e){
            $error[]=$fileName;
        }
    }



    protected function sendMail($ownerMail,$zipLink)
    {

        $email = new \PHPMailer();
        $email->IsSMTP();
        $email->SMTPAuth = true;
        $email->Host = $this->cont->getParameter('mailer_host');
        $email->Port = $this->cont->getParameter('mailer_port');
        $email->SMTPSecure = $this->cont->getParameter('mailer_encrypt');
        $email->Username = $this->cont->getParameter('mailer_user');
        $email->Password = $this->cont->getParameter('mailer_password');

        $email->setFrom($ownerMail);
        $email->addAddress($ownerMail);
        $email->addCC($ownerMail);
        $email->Body=$zipLink;
        $email->Subject="Toplu Dosya İndirme";
        $email->send();

        return new Response('OK');



    }





}