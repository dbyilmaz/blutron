<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 15.05.2017
 * Time: 16:31
 */

namespace ApiBundle\Services;


use AppBundle\Entity\Enduser;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\FinishRequestEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class HttpResponseLog
{
    const STATUS_OK='200';
    protected $cont;

    function __construct(ContainerInterface $container)
    {
        $this->cont=$container;
    }

    public function test(FilterResponseEvent $event)
    {

        $request = $event->getRequest();
        $response = $event->getResponse();

        $uri = $request->getRequestUri();
        $date = new \DateTime();
        $date=$date->format('d.m.Y@h:i:s');
        $ip = $request->getClientIp();
        $status = $response->getStatusCode();

        if ($status!= self::STATUS_OK&&$uri!='/s/logout') {

            $user=$this->cont->get('security.authorization_checker')->isGranted('ROLE_USER');
            if ($user)
            {
                $user=$this->cont->get('security.token_storage')->getToken()->getUser()->getMail();
            }
            else
            {
                $user="Anonim";
            }

            $log = sprintf('%s %s %s %s %s', $status, $uri, $date, $ip, $user);

            /**
             * Log kayıt  işlemleri..
             */
            $path=sprintf('%s%s',$this->cont->get('kernel')->getRootDir() ,$this->cont->getParameter('http_log'));

            file_put_contents($path, $log . PHP_EOL, FILE_APPEND | LOCK_EX);
        }
    }

}