<?php

namespace ApiBundle\Controller;

use AppBundle\Entity\CloudFile;
use AppBundle\Entity\Enduser;
use AppBundle\Services\EnduserPreferences;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\BCryptPasswordEncoder;

class RestApiController extends Controller
{
    const STATUS_OK='200';
    const STATUS_UNAUTHORIZED ='401';

    public function jsonFilesInfoAction(Request $request)
    {

        /**
         * @var Enduser $user
         */
        $user=$this->verifyUser($request);

        if ($user) {
            $filesArray = [];
            /**
             * @var $files CloudFile
             */
            $files = $this->getDoctrine()->getRepository('AppBundle:CloudFile')->findBy(
                array('user' => $user->getId()),
                array('uploadedAt' => 'DESC')
            );

            foreach ($files as $files) {
                $filesArray[] = array(
                    'uploaded_at' => $files->getUploadedAt(),
                    'file_size' => $files->getFileSize(),
                    's3_path' => sprintf("%s%s/%s", $this->getParameter('aws_url'), $this->getParameter('aws_bucket'), $files->getUniqName()),
                );
            }

            return new JsonResponse($filesArray, self::STATUS_OK);

        } else {
            return new JsonResponse([], self::STATUS_UNAUTHORIZED);
        }

    }



    public function quotaAction(Request $request)
    {
        /**
         * @var Enduser $user
         */
        $user=$this->verifyUser($request);

        if ($user)
        {
            $filesArray = [];
            /**
             * @var $files CloudFile
             */
            $files = $this->getDoctrine()->getRepository('AppBundle:CloudFile')->findBy(
                array('user' => $user->getId())
            );
            $countFiles=count($files);
            $fileSize=0;

            /** @var CloudFile $files*/
            foreach ($files as $files) {
                $fileSize=$fileSize+($files->getFileSize()) ;

            }
            $sumFileSize=round(($fileSize/1024),5);

            $infoCloudFiles=array(
                'file_count'=>$countFiles,
                'disk_usage'=>$fileSize
            );

            return new JsonResponse($infoCloudFiles,self::STATUS_OK);
        }

        return new JsonResponse([],self::STATUS_UNAUTHORIZED);
    }





    protected function verifyUser($request)
    {

        $params = [];

        /**
         * Gelen json formatındaki içeriği çözüyor.
         * @var Request $request
         */
        if ($content = $request->getContent()) {
            $params = json_decode($content, true);
            if(!empty($params)) {
                $username = $params['username'];
                $pass = $params['pass'];

                $userInfo = array(
                    'username' => $username,
                );

                /**
                 * @var  $user Enduser
                 */
                $user = $this->getDoctrine()->getRepository('AppBundle:Enduser')->findOneBy(array('username' => $username));

                if (!is_null($user)) {


                    /**
                     * Şifrenin doğrulanma işlemini gerçekleştiriyor.
                     */
                    $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
                    $isValid = $encoder->isPasswordValid($user->getPassword(), $pass, $user->getSalt());

                    if ($isValid) {

                        $this->login($user);
                        $apiEnabled = $this->get('app.enduser_preferences')->getPreference(EnduserPreferences::API_ENABLED, $user->getUsername());
                        if($apiEnabled)
                        {
                            $request->getSession()->invalidate(1);
                            return $user;
                        }
                        $request->getSession()->invalidate(1);
                        return false;
                    }
                    $request->getSession()->invalidate(1);
                    return false;

                }
                $request->getSession()->invalidate(1);
                return false ;

            }
            $request->getSession()->invalidate(1);
            return false;

        }




    }



    protected  function login($user)
    {
        /**
         * Otomatik Login.
         */
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);
        $this->get('session')->set('_security_main', serialize($token));
        return true;
    }


}
