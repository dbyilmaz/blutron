<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 19.05.2017
 * Time: 16:53
 */

namespace Tests\AppBundle\Controller;


use AppBundle\AppBundle;
use AppBundle\Command\BatchDownloadDispatcherCommand;
use AppBundle\Entity\BatchDownloadRequest;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Tests\Functional\app\AppKernel;
use Symfony\Component\Console\Tester\CommandTester;

class TestSymfonyCommand extends  WebTestCase
{
    public function testComm()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        $application =new Application($kernel);
        $application->add(new BatchDownloadDispatcherCommand());

        $command = $application->find('BatchDownloadDispatcher');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array('command' => $command->getName()));

        /**
         * @var BatchDownloadRequest $batchFiles
         */
        $batchFiles= $kernel->getContainer()->get('doctrine')->getRepository('AppBundle:BatchDownloadRequest')->findAll();

        foreach ($batchFiles as $bf)
        {
            if($bf->getStatus()==0)
            {
                $status=0;
                break;
            }

            $status=1;
        }

        $this->assertEquals(1,$status);
    }
}