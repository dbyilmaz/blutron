<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 19.05.2017
 * Time: 16:11
 */

namespace Tests\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestUserLoginPage extends  WebTestCase
{
    #doğru bilgiler.
    const EMAIL='m@gmail.com';
    const USERNAME='mehmet';
    const PASS='1';

    #hatalı bilgiler.
    const WEMAIL='mh@gmail.com';
    const WUSERNAME='mehmett';
    const WPASS='1111';

    public function testLoginUsername()
    {

        $client=$this->login(self::USERNAME,self::PASS);
        $name=$this->findName($client);

        /**
         * Kullanıcının adını soyadını arıyor.
         */
        $this->assertContains($name, $client->getResponse()->getContent());

    }

    public function testLoginEmail()
    {
        $client=$this->login(self::EMAIL,self::PASS);
        $name=$this->findName($client);

        /**
         * Kullanıcının adını soyadını arıyor.
         */
        $this->assertContains($name, $client->getResponse()->getContent());
    }



    public function testWrongUserPassword()
    {

        $client=$this->login(self::WUSERNAME,self::WPASS);
        $crawler=$client->request('GET','/');
        /*
         * dropdown toggle ancak giriş yapmış kullanıcılar için oluşuyor.
         * Ayrıca isimde eğer kullanıcı giriş yapmışsa orda bulunuyor.
         */
        $this->assertEquals(0,$crawler->filter('.dropdown-toggle')->count());
    }


    public function testWrongEmail()
    {
        $client=$this->login(self::WEMAIL,self::PASS);
        $crawler=$client->request('GET','/');
        /*
         * dropdown toggle ancak giriş yapmış kullanıcılar için oluşuyor.
         * Ayrıca isimde eğer kullanıcı giriş yapmışsa orda bulunuyor.
         */
        $this->assertEquals(0,$crawler->filter('.dropdown-toggle')->count());
    }



    protected function findName($client)
    {
        /**
         * Tekrar gelen içeriği istiyor.
         * @var Enduser $user
         */
        $crawler = $client->request('GET', '/');
        $user = static::$kernel->getContainer()->get('doctrine')->getRepository('AppBundle:Enduser')
            ->findOneBy(array('username'=>self::USERNAME));
        if($user)
        {
            return $user->getName();

        }
        return null;
    }

    protected function login($user,$pass)
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        /**
         * Form
         */
        $form = $crawler->filter('form[name=user_login]')->form();
        $form['user_login[username]'] = $user;
        $form['user_login[password]'] = $pass;
        $crawler = $client->submit($form);
        return $client;

    }
}