<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 18.05.2017
 * Time: 14:55
 */

namespace Tests\AppBundle\Controller;

use AppBundle\Entity\Enduser;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestHomePage  extends WebTestCase
{
    public function testNotLogin()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        $this->assertEquals(1, $crawler->filter('form[name=user_login]')->count());

    }


    public function testLogin()
    {
        $username='mehmet';
        $pass=1;

        $client = static::createClient();

        $crawler = $client->request('GET', '/');

        /**
         * Form
         */
        $form = $crawler->filter('form[name=user_login]')->form();
        $form['user_login[username]'] = $username;
        $form['user_login[password]'] = $pass;
        $crawler = $client->submit($form);

        /**
         * Tekrar gelen içeriği istiyor.
         * @var Enduser $user
         */
        $crawler = $client->request('GET', '/');
        $user = static::$kernel->getContainer()->get('doctrine')->getRepository('AppBundle:Enduser')
                               ->findOneBy(array('username'=>$username));
        $name=$user->getName();

        /**
         * Kullanıcının adını soyadını içerir.
         */
        $this->assertContains($name, $client->getResponse()->getContent());

        /*
         * Login formunu içermez.
         */
        $this->assertEquals(0, $crawler->filter('form[name=user_login]')->count());


    }
}