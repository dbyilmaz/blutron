<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 20.05.2017
 * Time: 16:05
 */

namespace Tests\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestUserRegister extends WebTestCase
{
    /*
     * Hata Mesajları
     */
    const FAIL_NAME="Ad Soyad Kısmını Boş Bırakamassınız.";
    const FAIL_MAIL="Email Kısmını Boş Bırakamassınız.";
    const FAIL_MAIDENNAME="Evlenmeden Önceki Soyadı Kısmını Boş Bırakamazsınız.";
    const FAIL_NOT_UNIQ_USERNAME="Girmiş olduğunuz kullanıcı adı kullanılmaktadır.";
    const FAIL_NOT_UNIQ_MAIL="Girmiş olduğunuz email kullanılmaktadır.";

    /*
     * Kullanıcı Bilgileri
     */
    const NAME="Mehmet Demir";
    const USERNAME="mehmetdemir";
    const MAIL="mehmetdemir@mehmetdemir.com";
    const PASS="1";
    const SEX=array(false,true);

    /*
     * Kaydı olmayan username ve email.
     */
    const UNİQ_USERNAME="burhanyilmaz";
    const UNIQ_EMAIL="by@g.com";


    public function testBlankName()
    {
        /*
         * Kullanıcı adı boş bırakıldığında .
         */
        $client=$this->form(null,self::USERNAME,self::MAIL,self::PASS,self::SEX[1],null)[0];
        $this->assertContains(self::FAIL_NAME, $client->getResponse()->getContent());

    }


    public  function testBlankMail()
    {

        /*
         * Mail boş bırakıldığında .
         */
        $client=$this->form(self::NAME,self::USERNAME,null,self::PASS,self::SEX[1],null)[0];
        $this->assertContains(self::FAIL_MAIL, $client->getResponse()->getContent());
    }





    public function testBlankMaidenname()
    {
        /*
         * Cinsiyet kadın seçildiğinde maidenname boş bırakılırsa.
         */
        $client=$this->form(self::NAME,self::USERNAME,self::MAIL,self::PASS,self::SEX[0],null)[0];

        $this->assertContains(self::FAIL_MAIDENNAME, $client->getResponse()->getContent());
    }


    public function testUniqUserName()
    {
        /*
         * Veritabanında bulunan bir username girince.
         */
        $client=$this->form(self::NAME,self::USERNAME,self::UNIQ_EMAIL,self::PASS,self::SEX[1],null)[0];

        $this->assertContains(self::FAIL_NOT_UNIQ_USERNAME, $client->getResponse()->getContent());

    }




    public function testUniqEmail()
    {

        /*
         * Veritabanında bulunan bir email girilince.
         */
        $client=$this->form(self::NAME,self::UNİQ_USERNAME,self::MAIL,self::PASS,self::SEX[1],null)[0];

        $this->assertContains(self::FAIL_NOT_UNIQ_MAIL, $client->getResponse()->getContent());

    }





    public function testCityList()
    {

        $client = static::createClient();

        $crawler = $client->request('GET', '/register');

        /*
         * Formun içindeki illeri $city arrayında tutacak.
         */
        $city=array();
        $crawler=$crawler->filter('#user_register_city option');

        foreach ($crawler as $c)
        {
            $city[]=($c->textContent);
        }

        /*
         * Verilen şehirle linkinden iller çekilere $cityList dizisine aktarılıyor.
         */
        $cityList=$this->csvReader();

        /*
         * İki dizinin eşleşmesi kontrol ediliyor.
         */
        if ($city==$cityList)
        {
            $match=true;
        }
        else
        {
            $match=false;
        }

        $this->assertEquals(true,$match);
    }


    protected function  csvReader()
    {


        $url = "https://commondatastorage.googleapis.com/ckannet-storage/2012-07-19T130158/cities-of-Turkey---Sheet1.csv";
        $csvData = file_get_contents($url);
        $lines = explode(PHP_EOL, $csvData);
        $array = array();

        foreach ($lines as $line => $value) {
            $array[] = str_getcsv($value);
        }

        $il=array();
        for ($i=1;$i<count($array);$i++)
        {
            if(isset($array[$i][2]))
            {
                $il[]=$array[$i][2];
            }
        }


        return $il;
    }


    protected function form($name=null,$username=null,$mail=null,$pass=null,$sex=null,$maiden=null)
    {

        $client = static::createClient();

        $crawler = $client->request('GET', '/register');

        $info=array();
        /**
         * Form
         */
        $form = $crawler->filter('form[name=user_register]')->form();
        $form['user_register[name]'] = $name;
        $form['user_register[username]'] = $username;
        $form['user_register[mail]'] = $mail;
        $form['user_register[password][first]'] = $pass;
        $form['user_register[password][second]'] = $pass;
        $form['user_register[sex]']->select($sex);
        $form['user_register[maidenName]']=$maiden;
        $crawler = $client->submit($form);

        $info[]=$client;
        $info[]=$crawler;
        return $info;

    }

}