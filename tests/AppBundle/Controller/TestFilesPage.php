<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 18.05.2017
 * Time: 17:41
 */

namespace Tests\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TestFilesPage extends WebTestCase
{
    const STATUS_CODE=302;
    const STATUS_OK=200;
    const STATUS_NOT_FOUND=404;

    const FILE_COUNT="3 dosyanız";
    const FILES='/s/cloud';
    const DELETE_FILES_PATH="/s/delete/33";//Farklı bir kullanıcıya ait.


    /**
     * @test testLinkActiveNotLogin
     */
    public function testLinkActiveNotLogin()
    {

        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertEquals(0,$crawler->filter('li.active:contains("Anasayfa")')->count());

        $crawler=$client->request('GET',self::FILES);

        $statusCode=$client->getResponse()->getStatusCode();
        $this->assertEquals(self::STATUS_CODE,$statusCode);

    }



    public function testLinkActiveLogin()
    {
        $client=$this->login();

        /*
         * Anasayfa active link test
         */

        $crawler = $client->request('GET', '/');
        $this->assertEquals(0,$crawler->filter('li.active:contains("Anasayfa")')->count());

    }



    public function testFilePage()
    {

        $client=$this->login();

        /**
         * Dosyalar sayfası test
         */

        $crawler=$client->request('GET',self::FILES);
        $statusCode=$client->getResponse()->getStatusCode();
        $this->assertEquals(self::STATUS_OK,$statusCode);



    }



    public function testFileList()
    {
        $client=$this->login();

        /*
         * Dosya Listeleme
         */


        $crawler=$client->request('GET',self::FILES);
        $countFiles=$crawler->filter('.btn.gndr:contains("Gönder")')->count();
        $this->assertEquals(3,$countFiles);

        $this->assertContains(self::FILE_COUNT, $crawler->filter('.panel-footer')->text());


    }



    public function testMatchFileSize()
    {

        $client=$this->login();

        $crawler=$client->request('GET',self::FILES);


        /**
         *Toplam dosya boyutunun listelenler ile eşlemesi
         */
        $text=$crawler->filter('#filesize')->slice(0);
        $fileSize=array();
        $sumFileSize=0;

        foreach ($text as $t)
        {
            $fileSize[]=explode(" ",$t->textContent);
        }

        for($i=0;$i<count($fileSize);$i++)
        {
            $sumFileSize=$sumFileSize+$fileSize[$i][0];
        }

        $fileSizeArray=explode(" ",$crawler->filter('.sumFileSize')->text());
        $this->assertEquals($sumFileSize,$fileSizeArray[1]);

    }



    public function testDeleteFile()
    {

        $client=$this->login();
        $crawler=$client->request('GET',self::FILES);


        /**
         * Dosya silme ve daha sonrasında silinip silinmediğini kontrol etme.
         */
        $deleteLink = $crawler
            ->filter('a:contains("Evet")') // Evet sadece silmede bulunuyor.
            ->eq(1)
            ->link();


        $client->click($deleteLink);
        $crawler=$client->request('GET',self::FILES);


        $this->assertEquals(0,$crawler->filter('html:contains("'.$deleteLink->getUri().'")')->count());
    }



    public function testListFilesOtherUser()
    {
        /**
         * Başka kullanıcının dosyasını listeleyebiliyor mu?
         */

        $client=$this->login();
        $crawler=$client->request('GET',self::FILES);
        $otherUserFileId=33;

        $this->assertEquals(0,$crawler->filter(sprintf('#%s',$otherUserFileId))->count());
    }


    public function testDeleteFileOtherUser()
    {
        /*
         * Başkasının dosyasını sildiğin 500 hata kodu alacak
         */

        $client=$this->login();
        $crawler=$client->request('GET',self::DELETE_FILES_PATH);
        $statusCode=$client->getResponse()->getStatusCode();
        $this->assertEquals(self::STATUS_NOT_FOUND,$statusCode);
    }


    protected function login()
    {

        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        /**
         * Form
         */
        $form = $crawler->filter('form[name=user_login]')->form();
        $form['user_login[username]'] = 'mehmet';
        $form['user_login[password]'] = '1';
        $crawler = $client->submit($form);
        return $client;
    }


}