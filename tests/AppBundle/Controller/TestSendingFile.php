<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 20.05.2017
 * Time: 10:24
 */

namespace Tests\AppBundle\Controller;


use AppBundle\Command\SendMailCommand;
use AppBundle\Entity\PendingEmail;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Tester\CommandTester;

class TestSendingFile extends WebTestCase
{
    public function testSentFile()
    {
        $kernel = $this->createKernel();
        $kernel->boot();

        $application =new Application($kernel);
        $application->add(new SendMailCommand());

        $command = $application->find('SendMail');
        $commandTester = new CommandTester($command);
        $commandTester->execute(array('command' => $command->getName()));

        /**
         * @var PendingEmail $pendingEmail
         */
        $pendingEmail= $kernel->getContainer()->get('doctrine')->getRepository('AppBundle:PendingEmail')->findAll();

        foreach ($pendingEmail as $pE)
        {
            if($pE->getStatus()==0)
            {
                $status=0;
                break;
            }

            $status=1;
        }

        $this->assertEquals(1,$status);
    }

}