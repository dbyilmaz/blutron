<?php
/**
 * Created by PhpStorm.
 * User: dby
 * Date: 20.05.2017
 * Time: 10:47
 */

namespace Tests\ApiBundle\Controller;


use AppBundle\Entity\Enduser;
use AppBundle\Entity\CloudFile;
use GuzzleHttp\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;

class TestApi extends  WebTestCase
{
    const files='/api/files';
    const quota='/api/quota';

    const username='mehmet';
    const pass='1';

    const status_Unauthorized='401';
    const status_Ok='200';

    const json='application/json';
    const size=27;



    /*
     * Dosya listeleme
     */

    public function testListFile()
    {
        /*
         * Api_enabled false oluyor.
         */
        $this->apiStatus(false);

        /*
         * Response 'un status kodunu çeker.
         */
        $statusCode=$this->statusCode(self::files);

        /*
         * Gelen statuscode 401 e eşit mi?
         */
        $this->assertEquals(self::status_Unauthorized,$statusCode);
    }


    public function testQuotaFile()
    {
        /*
         * Api_enabled false oluyor.
         */
        $this->apiStatus(false);

        /*
         *Response 'un status kodunu çeker.
         */
        $statusCode=$this->statusCode(self::quota);


        /*
         * Gelen statuscode 401 e eşit mi?
         */
        $this->assertEquals(self::status_Unauthorized,$statusCode);
    }


    public function testEnableListFile()
    {

        /*
         * Api_enabled false oluyor ve veri tabanında kullanıcıya ait olan dosya sayını dönüyor.
         */
        $countFile=$this->apiStatus(true);

        /*
        *Response 'un status kodunu çeker.
        */
        $statusCode=$this->statusCode(self::files);

        /*
         *Response çeker.
         */
        $response=$this->Response(self::files);


        /*
         * Response tipini çeker.
         */
        $contentType=$response->headers->get('content-type');

        /*
         * Json da gelen dosya sayısını çeker.
         */
        $sumFiles=(count(json_decode((string)$response->getContent())));

        /*
         * Status  code 200 ile aynı mı ?
         */

        $this->assertEquals(self::status_Ok,$statusCode);


        /*
         * ContentType aplication/json mı ?
         */
        $this->assertEquals(self::json,$contentType);


        /*
         * Beklediğimiz dosya sayısı json da ve veritabanında aynı mı ?
         */
        $this->assertEquals($countFile,$sumFiles);
    }


    public function testEnableQuotaFile()
    {

        /*
         * Api_enabled false oluyor ve veri tabanında kullanıcıya ait olan dosya sayını dönüyor.
         */
        $countFile=$this->apiStatus(true);

        /*
        *Response 'un status kodunu çeker.
        */
        $statusCode=$this->statusCode(self::quota);

        /*
         *Response çeker.
         */
        $response=$this->Response(self::quota);



        /*
         * Gelen jsonı çözer.
         */
        $files=(json_decode((string)$response->getContent()));

        /*
         * Json daki toplam dosya boyutunu verir.
         */
        $file_count=$files->file_count;

        /*
         * Jsonda bulunan toplam dosya boyutunu çeker.
         */
        $all_size=$files->disk_usage;


        /*
         * Response tipini çeker.
         */
        $contentType=$response->headers->get('content-type');

        /*
         * Json da gelen dosya sayısını çeker.
         */
        $sumFiles=(count(json_decode((string)$response->getContent())));


        /*
         * Status  code 200 ile aynı mı ?
         */
        $this->assertEquals(self::status_Ok,$statusCode);

        /*
         * ContentType aplication/json mı ?
         */
        $this->assertEquals(self::json,$contentType);

        /*
         * Beklediğimiz dosya sayısı json da ve veritabanında aynı mı ?
         */
        $this->assertEquals($countFile,$file_count);


        /*
         * Beklediğimiz toplam dosya boyutu ile json da gelen toplam dosya boyutu aynımı.
         */
        $this->assertEquals(self::size,$all_size);

    }





    protected function apiStatus($bool)
    {

        $kernel = $this->createKernel();
        $kernel->boot();

        /**
         * @var Enduser $user
         */
        $user=$kernel->getContainer()->get('doctrine')->getRepository('AppBundle:Enduser')->findOneBy(array('username'=>self::username));

        if($user)
        {
            $preferences=array('sharing_enabled'=>true,'sharing_notification_enabled'=>true,'api_enabled'=>$bool);
            $user=$user->setPreferences($preferences);
            $em=$kernel->getContainer()->get('doctrine.orm.entity_manager');
            $em->persist($user);
            $em->flush();
        }

        /**
         * @var CloudFile $file
         */
        $file=$kernel->getContainer()->get('doctrine')->getRepository('AppBundle:CloudFile')->findBy(array('user'=>$user->getId()));
        return count($file);
    }


    protected function statusCode($uri)
    {


        $client = static::createClient(array(
            'environment' => 'test',
            'debug'       => true,
        ));
        $response=$client->request(
            'POST',
            $uri,
            array(),
            array(),
            array(
                'CONTENT_TYPE'          => 'application/json'),
                json_encode(['username' => self::username,'pass' => self::pass])

        );




            $statusCode=$client->getResponse()->getStatusCode();
       return $statusCode;
    }





    protected function Response($uri)
    {



        $client = static::createClient(array(
            'environment' => 'test',
            'debug'       => true,
        ));
        $response=$client->request(
            'POST',
            $uri,
            array(),
            array(),
            array(
                'CONTENT_TYPE'          => 'application/json'),
            json_encode(['username' => self::username,'pass' => self::pass])

        );

        return $client->getResponse();

    }



}
