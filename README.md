Blutron Projesi
========================

### 23.05.2017 Versiyon 1.1

:bust_in_silhouette: __Kullanıcı İşlemleri__
1. Kullanıcı __kaydı__ ve kullanıcı __girişi__ vardır.
1. Kullanıcı girişi yapmayan kullanıcılar sadece anasayfa dizinine ulaşır.
1. Kullanıcı girişi yapmayan kişiler __anasayfa ve dosyalar__ yazısını navbarda göremezler.
1. Kullanıcı girişi yapmayan kullanıcılar izinleri olmayan kısımlara girdiklerin anasayfaya yönlendirilir.
1. Kullanıcı girişi yapmayan kullanıcılar __isim ve çıkış kısımlarını__ göremezler.
1. Kullanıcı girişi yapmış olan kullanıcılar anasayfa dizinindeki __login formunu__ göremezler.
1. Kullanıcı girişi yapmış olan kullanıcılar __register formunu__ göremezler.
1. Kullanıcı girişi yapmış olan kullanıcılar navbarda __anasayfa yazısını__ göremezler.
1. Kullanıcı girişi yapmış olan kullanıcılar __dosyalar ve kullanıcı adı soyadı __ kısmını görürler.
1. Kullanıcı giriş yaptıktan sonra dosyalar sayfasına otomatik olarak yönlendirilirler.
1. Kullanıcı giriş yaptıktan sonra çıkış yapabilir. İsterse oturumunu açık bırakabilir.
1. Kullanıcı kaydını  geçerli şekilde yapan kullanıcılar otomatik giriş yaparlar.
1. Kullanıcı kaydı yaparken geçerli olmayan ve boş bırakılan alanlar için uyarı mesajı alırlar.
1. Kullanıcı kaydı kısmına giriş kısmına dönmelerini sağlayan buton eklenmiştir.
1. Kullanıcı __şifresini unuttum__ kısmından email ve kullanıcı adını girerek değiştirebilir.

___


:wrench: __Kullanıcı Ayarları__
1. __Api__ Ayarları
   1. Kullanıcın dosyalarına api ile erişimine izin verip vermeyeceğini belirler.
1. __Paylaşım__ Ayarları
   1. Kullanıcı kendisiyle dosya paylaşımına izin verip vermeyeceğini belirler.    
   1. Kullanıcı kendi paylaştığı dosyanın ilk defa indirilme durumu hakkında bilgi maili :envelope:  alıp almayacağını belirler.


___

 :file_folder: __Dosya İşlemleri__
1. Kullanıcı kendi alanına dosya __yükleyebilir__.
1. Yükelenen dosya S3'e yüklenir ve veritabanına bilgileri kaydedilir.
1. Kullanıcı dosyalarını __indirebilir__.
1. Kullanıcı dosyalarını __silebilir__.
1. Kullanıcı dosyayı sildiğinde dosya hem __S3__ ten hem de __veritabanında__` silinir.
1. Kullanıcı dosyalarını toplu bir şekilde indirebilir.
1. Toplu şekilde indirilen dosyalar hazır olduğunda kullanıcıya mailine __7 gün geçerli S3 linki__ yollanacaktır. 7 günün sonunda link ölecek ve dosya S3 ten otomatik silinecek.
1. Kullanıcı istediği dosyayı paylaşabilir ve gönderir.(Sadece paylaşıma açık kullanıcılara.)
1. Kullanıcı gönderdiği dosyayı süreli bir link ile gönderir. Süresi dolduğunda link ölür.
1. Gönderilen dosya kuyruğa eklenir. Sunucuda çalışan bir script ile __kuyrukta olan dosyalar her 5 dk bir gönderilir__  :clock3: .
1. Gönderilen dosyalarda bir sorun oluşursa tekrar gönderilmek üzere tutulur.
1. Dosya paylaşımı yapıldığı zaman eğer mail sahibi paylaşıma açık ve bl__tronda bulunuyorsa dosyayı kişinin mailine __ekte__ yollamaktadır.
1. Dosya paylaşımı yapılan kişi blutronda yoksa ve paylaşıma kapalı ise __uyarı mesajı__ verir.
1. Paylaşım ve gönderim yapılırken yazılan mail kontrol edilir geçerli değilse ve blutronda yoksa hata mesajı verir.
1. Başarılı bir şekilde paylaşılan dosya için kullanıcıya ulaşan linkten ilk defa indirildiğinde dosya sahibine bildirim maili gider.(Sadece dosya indirme bildirimi :bell:  açık olan kullanıcılar için.)

___


__API İşlemleri__
1. Sadece __POST__ işleminde çalışır.
1. Kullanıcı doğrulaması ister.
1. Kullanıcı adı ve şifresi yanlış olduğu durumda __401 hatası__ verir ve boş array döner.
1. Kullanıcı adı ve şifre doğrulandığında kullanıcıya __200 durum kodu__ döner.
1. Files ve Quota işlemleri için kullanıcı doğrulamasından geçmiş olması gerekir.
1. __Files işlemi__ kullanıcının blutron hesabında bulunan dosyaları listeler.Dosyaların,
   1. Yuklenme tarihini
   1. Boyutunu
   1. S3 Linkini  verir.
1. __Quota işlemi__ kullanıcın kaç dosyasının olduğunu ve toplam boyutunu verir.

___

   
:camera: **Ekran Görüntüleri**
 
 __Giriş Sayfası__
 
 ![](web/images/Readme Images/log.png)
 
 
 __Kayıt Sayfası__
 
 ![](web/images/Readme Images/register.png)
 
 
 __Şifremi Unuttum Sayfası__
 
 ![](web/images/Readme Images/passchange.png)
 
 
 __Giriş Yapmış Kullanıcı Anasayfası__
 
 ![](web/images/Readme Images/authhome.png)
 
 
 __Dosyalar Sayfası__
 
 ![](web/images/Readme Images/filesandupload.png)
 
 
__Dosya Silme__
 
 ![](web/images/Readme Images/delete.png)
 
 
 __Dosya Toplu İndirme__
 
 ![](web/images/Readme Images/down.png)
 
 
 __Dosya Paylaşma__
 
 ![](web/images/Readme Images/share.png)
 
 
 __Dosya Gönderme__
 
 ![](web/images/Readme Images/sent.png)
 
 
 __Kullanıcı Ayarları Sayfası__
 
 ![](web/images/Readme Images/setting.png)